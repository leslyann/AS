% jeu de donnees
clear all
load donnees_regress_rob.mat
robuste = 1; % 0 si on ne veut pas d'estimation robuste
% Nombre d'elements
N=numel(X);
% Moyennes en X et Y
mX=mean(X);
mY=mean(Y);
% Variance, Covariance X,Y
covXY=sum((X-mX).*(Y-mY))/(N-1);
varX=sum((X-mX).*(X-mX))/(N-1);
varY=sum((Y-mY).*(Y-mY))/(N-1);
% Ecart Type
etX=sqrt(varX);
etY=sqrt(varY);
% Correlation
r=covXY/(etX*etY);

% Initialisation des poids
poids=ones(size(X));
sigma=1;sigma2=sigma*sigma;

for i=1:500
    % Estimation de alpha et beta
    % A REMPLIR
if robuste==0
    poids=ones(size(poids));
else
% Mise � jour des poids
fonction_de_cout=(Y-alpha*X-beta).*(Y-alpha*X-beta);
poids=(2/sigma2)*exp(-fonction_de_cout/sigma2);
end    
end

% Calcul de param�tres intermediaires
SCT = sum((Y-mY).*(Y-mY));
SCE = alpha*alpha*sum((X-mX).*(X-mX));
SCR = sum((Y-alpha*X-beta).*(Y-alpha*X-beta));

% Affichage graphique
figure
scatter(X,Y,'+','LineWidth',2);
Xmin=min(X);Xmax=max(X);
x=Xmin:(Xmax-Xmin)/100:Xmax;
y=alpha*x+beta;
hold on;
plot(x,y,'r','LineWidth',2);
titre=sprintf('alpha estime = %.3f    beta estime = %.3f',alpha,beta);
title(titre);

% Affichage ecran
fprintf('- Correlation = %.5f\n',r);
fprintf('- Estimation de alpha, beta = (%.4f,%.4f)\n',alpha,beta);
fprintf('- Erreur r�siduelle (totale et normalis�e) = (%.4f,%.4f)\n',SCR,SCR/(N-1));
fprintf('- Verification que correlation =sqrt(SCE/SCT): %.5f\n',sqrt(SCE/SCT));
fprintf('************\n');
if robuste==1
figure
scatter(X,Y,30,poids/2);title('poids associ�s');
end
