function covXY = covariance( X, Y )

covXY = mean(X - mean(X))*mean(Y - mean(Y))

end

