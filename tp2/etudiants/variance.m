function v = variance( X )
p=size(X);
v=(X'*X)
v=(1./(p-1)).*v;
end