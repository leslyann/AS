function random_data=donnees_aleatoires(N, moyenne, ecart_type)

X = randn(N,1);

random_data = sqrt(ecart_type)*X+moyenne;

end