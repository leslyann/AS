clear all

a1_true=3;
a2_true=-1;
a3_true=-2;
a4_true=2;
b_true=5;

bruit=04.00;


% Creation des X
X1=2*randn([1,1000])+1;
X2=randn([1,1000])+2;
X3=4*randn([1,1000])+1;
X4=0.2*randn([1,1000])+0.4;
% Creation des Y
Y=a1_true*X1+a2_true*X2+a3_true*X3+a4_true*X4+b_true+randn(size(X1))*bruit;

% Nombre d'elements
N=numel(X1);

A=zeros(5);
B=zeros(5,1);
for i=1:N
    vecX=[X1(i),X2(i),X3(i),X4(i),1]';
    % Remplir ICI
    A=;
    B=;
end

solution=inv(A)*B;
a1=solution(1);
a2=solution(2);
a3=solution(3);
a4=solution(4);
b=solution(5);


erreur=Y-(a1*X1+a2*X2+a3*X3+a4*X4+b);
err=sum(erreur.*erreur);

% Affichage ecran
fprintf('************\n- Vraies valeurs de A et b: \t(%.3f\t%.3f\t%.3f\t%.3f\t%.3f), buit = %.3f\n',a1_true,a2_true,a3_true,a4_true,b_true,bruit);
fprintf('- Estimation de A et b: \t(%.3f\t%.3f\t%.3f\t%.3f\t%.3f)\n',a1,a2,a3,a4,b);
fprintf('- Erreur résiduelle (totale et normalisée) = (%.4f,%.4f)\n',err,err/(N-1));
fprintf('************\n');
