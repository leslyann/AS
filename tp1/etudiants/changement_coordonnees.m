function nouvelles_coordonnees=changement_coordonnees(X, pourcentage, reduction)

if nargin == 2
    reduction = 0;
elseif nargin == 1
    pourcentage = 0.9;
end
    
%Vecteurs propres

[V,D]=notre_acp(X, reduction);

%Base de projection

[M]=base_projection(V,D,pourcentage);

%Projection

nouvelles_coordonnees=projection(M, X);

end