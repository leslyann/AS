%function donnees_centrees_reduites=centrer_reduire(X,reduire)
% defaut reduire=1

function donnees_centrees_reduites=centrer_reduire(X,reduire)

meanX = mean(X);
stdX = std(X);
crX = X;

for i=1:1:size(X,1),
    crX(i,:) = (X(i,:)-meanX);
    if reduire
        crX(i, :) = (crX(i,:)./stdX);
    end
end
donnees_centrees_reduites=crX;