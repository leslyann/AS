% Fichier de test "test.m"

% Graphique

% D�efinition de t avec un pas de 0.2
t=  0:0.2:5;
%D�finition de f1
f1= cos(2*pi*t);
%Affichage : on nettoie la fen�tre graphique courante puis on affiche clf
clf;
plot(t,f1)
% On peut aussi cr�er une nouvelle figure
figure;
plot(t,f1)

% D�finition de f2
f2 = cos(2*pi*t+1);
%Affichage en rouge
plot(t,f2,'r')

% On peut aussi afficher 2 courbes sur la m�me figure
figure;
plot(t,f1)
% On superpose les courbes sur la m�me figure
hold on
plot(t,f2,'r')
% On arr�te de superposer les courbes
hold off
nom_titre='figure de test';
title(nom_titre)

% fermeture de toutes les fenetres graphiques
%close all;
%sous figures :
figure;

subplot(2,3,1);plot(t,f1,'+');title('Fig 1');
subplot(2,3,2);plot(t,f1,'b');title('Fig 2');
subplot(2,3,3);plot(t,f1,'r');title('Fig 3');
subplot(2,3,4);plot(t,f2,'g');title('Fig 4');
subplot(2,3,5);plot(t,f2,'m');title('Fig 5');
subplot(2,3,6);plot(t,f2,'o');title('Fig 6');

% Affichage de valeurs numeriques

i=4;f=sqrt(10);
fprintf('la valeur \n\tentiere de i est %d\n\td�cimale (2 chiffres) de f: %.2f \n\td�cimale (3 chiffres) de f: %.3f\n',i,f,f); 
