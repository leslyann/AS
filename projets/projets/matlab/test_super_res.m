clear all;
load data_projet.mat;
addpath ./svr
addpath ../../../libsvm/matlab
%%%%%%%%%%%%%%%%%%%%%%% PARAMS %%%%%%%%%%%%%%%%%%%%%%%
% 0 = SVM
%-1 = KNN
% 1 = SVM - spectre
param.method = 0;
% SVM
param.gamma = 1;
% KNN
param.k = 3;
param.d = 2;
% SVM - spectre
param.val_sigma = 0:0.5:15;
param.acp = 1;
% Images
size_lr = 10;
size_hr = 20;
nb = 10;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

[images_superresolues] = super_resolution(images_apprentissage_lr(1:size_lr,1:size_lr,1:nb),images_apprentissage_hr(1:size_hr,1:size_hr,1:nb), images_test_lr(1:size_lr,1:size_lr,1:1), param)
%[images_superresolues] = super_resolution(images_apprentissage_lr(:,:,1:nb),images_apprentissage_hr(:,:,1:nb), images_test_lr(:,:,1:1), param)
%[images_superresolues] = super_resolution(images_apprentissage_lr,images_apprentissage_hr, images_test_lr, param)

% Affichage des images d'apprentissage basse résolution
figure(1);
imagesc(images_apprentissage_lr(1:size_lr,1:size_lr))
% Affichage des images d'apprentissage haute résolution
figure(2);
imagesc(images_apprentissage_hr(1:size_hr,1:size_hr))

% Affichage des images test basse résolution
figure(3);
imagesc(images_test_lr(1:size_lr,1:size_lr))
% Affichage des images test haute résolution
figure(4);
%imagesc(images_superresolues(1:size_hr,1:size_hr))
imagesc(images_superresolues)