function [M]= base_projection(V, D, pourcentage)
val = diag (D);
expl = cumsum(val/sum(val));
i = 1;
while expl(i) <= pourcentage,
    i=i+1;
end
i
[M]=V(:,1:i);
plot(diag(D)/diag(D));