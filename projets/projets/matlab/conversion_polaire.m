function [rho,theta] = conversion_polaire(x,y)
%  [rho,theta] = conversion_polaire(x,y)
% Fonction de test qui convertit des coordonnees cartesiennes en polaire

% Le module rho est la racine carree de x^2+y^2

rho = sqrt(x*x+y*y);

% L'argument est l'arc tangente de y/x
 
theta = atan(y/x);

