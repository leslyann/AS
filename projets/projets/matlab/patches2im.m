% function im=patches2im(imP)
% retrieve the image after the im2patches transformation
function im=patches2im(imP,nbl,nbc);

[nl,nc]=size(imP);
k=(nc+1)/2;
im=reshape(imP(:,k:k),[nbl,nbc]);