%function donnees_centrees_reduites=centrer_reduire(X,reduire)
% defaut reduire=1

function [V,D]=notre_acp(X,reduire)
[crX,a,b] = centre_donnees(X);
[W,C] = eig(crX'*crX);
if C(1,1) < C(size(C,1),size(C,2))
    V=flip(W,2);
    D=flip(flip(C, 1),2);
end
