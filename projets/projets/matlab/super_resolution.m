% function images_superresolues = super_resolution(ilbr,ilhr, images_test, param)
%
% ilbr : images_learning_basse_resolution, utilisées pour l'apprentissage
% ilhr : images_learning_haute_resolution, utilisées pour l'apprentissage
% images_test : images dont on veut augmenter la résolution
%
% param.method : 0 = SVR & 1 = knn
% param.gamma : 0 = validation croisée, sinon gamma utilisé pour l'apprentissage

function [images_superresolues] = super_resolution(ilbr,ilhr, images_test, param)
r = 2;
nbfolds = 10;

% Transformation des images en vecteurs
disp ("im2patches...")
fflush(stdout);
ilbr_resize=imresize(ilbr,2);
vec_ilbr=im2patches(ilbr_resize,r,0);
%vec_ilbr=im2patches(ilbr,r,0);
vec_ilhr=im2patches(ilhr,r,0);
images_test_resize=imresize(images_test,2);
[nbl,nbc,nbim]=size(images_test_resize);
vec_imt=im2patches(images_test_resize,r,0);
%[nbl,nbc,nbim]=size(ilhr);
%vec_imt=im2patches(images_test,r,0);
disp ("done")
fflush(stdout);

if param.method == 0 % Methode SVR
    options=init_options_regression;
    if param.gamma == 0 % Validation croisee
       % On fera varier γ dans la gamme [0.1, 0.5, 1, 2, 5, 10, 15, 20].
       EQMmin = inf;
       disp("validation croisée...")
       fflush(stdout);
       
       for g=[0.1 0.5 1 2 5 10 15 20]
           disp("gamma=")
           disp(g)
           fflush(stdout);
           EQM = 0;
           options.kernel_d=g;
           [label_kt,label_kv,data_kt,data_kv]=decoupe_data(vec_ilbr,vec_ilhr,nbfolds);
           for i=1:nbfolds
              [interpol,corr]=svm_regression(data_kv{i},label_kt{i},data_kt{i},options,label_kv{i});
              EQM=EQM+sqrt(1/size(label_kv{i}(:),1)*abs(label_kv{i}-interpol).^2);
           end
           if EQMmin > EQM
               EQMmin = EQM;
               gamma = g;
           end
       end
       % On fixe gamma
       disp("final gamma=")
       disp(g)
       fflush(stdout);
       options.kernel_d=gamma;
       
       disp("done")
       fflush(stdout);
    else
        options.kernel_d=param.gamma;   
    end
        % On lance svm sur les images de test
        disp("svm_regression...")
        fflush(stdout);
        images_int = svm_regression(vec_imt,vec_ilhr,vec_ilbr,options);
        %images_superresolues=images_int;
        images_superresolues = patches2im(images_int,nbl,nbc);
        disp("done")
        fflush(stdout);
        
elseif param.method == -1 % Methode knn
    if param.k == 0
        k=0
        i=0
       % Validation croisee
       % On fera varier k dans la gamme [1 2 3 4 5].
       EQMmin = inf;
       %disp("validation croisée...")
       %fflush(stdout);

       for ki=[1 2 3 4 5]
           EQM = 0;
           [label_kt,label_kv,data_kt,data_kv]=decoupe_data(vec_ilbr,vec_ilhr,nbfolds);
           for i=1:nbfolds
              [classified, k, dist, idx] = fastKNN(trained, unknown, k, distance)
              EQM=EQM+sqrt(1/size(label_kv{i}(:),1)*abs(label_kv{i}-interpol).^2);
           end
           if EQMmin > EQM
              EQMmin = EQM;
              k = ki;
           end
       end
       param.k = k;
       disp("done")
       fflush(stdout);
    end
        % On lance knn sur les images de test
        disp("knn...")
        fflush(stdout);
        classified = fastKNN(vec_ilbr, vec_ilhr, vec_imt, param.k, param.d)
        disp("done")
        fflush(stdout);

else %spectres
    sp_br = spectres_images(ilbr,param.val_sigma);
    sp_hr = spectres_images(ilhr,param.val_sigma);
    sp_test = spectres_images(images_test,param.val_sigma);
    if param.acp == 1
        nsp_br=changement_coordonnees(sp_br, .8, false);
        nsp_hr=changement_coordonnees(sp_hr, .8, false);
        nsp_test=changement_coordonnees(sp_test, .8, false);
        imagesc(nsp_br);
    end
end
