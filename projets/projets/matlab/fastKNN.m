% The MIT License (MIT)
%
% Copyright (c) 2016 Markus Bergholz
% https://github.com/markuman/fastKNN

function [classified, k, dist, idx] = fastKNN(trained, unknown, k, distance)

    if (nargin <= 3)
        % Minkowski Distance
        % for p == 2, Minkowski becomes equal Euclidean
        % for p == 1, Minkowski becomes equal city block metric
        % for p ~= 1 && p ~= 2 -> Minkowski https://en.wikipedia.org/wiki/Minkowski_distance
        distance = 2;
    end

    % trained data has one more column as unknown, the class
    [dist, idx] = getDistance(trained, unknown, distance);

    if (nargin <= 2)
        % determine k value when no one is given
        % possible number of categories + 1
        k = numel( unique( trained(:,end) ) ) + 1;
    end
    
    classified  = mode(trained(idx(1:k), end));


end % function fastKNN

function [values, idx] = getDistance(v1, v2, p)
    [values, idx] = sort (sum ( abs( (v1(:,1:end-1) - v2(ones(size(x,1),1), :) ) .^p), 2) .^ (1 / p));
end